﻿namespace Domain.Dtos
{
    public class BetDto
    {
        public int RuletaId { get; set; }
        public int Number { get; set; }
        public string Colour { get; set; }
        public int Money { get; set; }
    }
}
