﻿namespace Domain.Models
{
    public class BetRuleta
    {
        public Ruleta Ruleta { get; set; }
        public Bet Bet { get; set; }
        public bool Winner { get; set; }
        public float Money { get; set; }
    }
}
