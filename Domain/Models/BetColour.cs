﻿namespace Domain.Models
{
    public enum BetColour
    {
        BlackBet = 1,
        RedBet = 2
    }
}
