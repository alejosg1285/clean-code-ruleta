﻿using System;

namespace Domain.Models
{
    public class Ruleta
    {
        public int Id { get; set; }
        public bool IsOpen { get; set; }

        public static int GetWinnerNumber()
        {
            Random rnd = new Random();
            int winner = rnd.Next(0, 37);

            return winner;
        }
    }
}
