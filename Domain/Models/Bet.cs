﻿namespace Domain.Models
{
    public class Bet
    {
        public int Id { get; set; }
        public Ruleta Ruleta { get; set; }
        public BetType Type { get; set; }
        public int Number { get; set; }
        public BetColour Colour { get; set; }
        public int Money { get; set; }
        public int UserId { get; set; }

        public static BetType GetBetType(int number, string colour)
        {
            if (number != 0) return BetType.BetNumber;

            return BetType.BetColout;
        }

        public static BetColour GetBetColour(string colour)
        {
            if (colour.Equals("Black")) return BetColour.BlackBet;

            return BetColour.RedBet;
        }

        public float BetMoneyWin(int winnerNumber)
        {
            var winMoney = 0.0f;
            if (winnerNumber == Number)
            {
                if (Type == BetType.BetColout)
                    winMoney = Money * 1.8f;
                else
                    winMoney = Money * 5.0f;
            }

            return winMoney;
        }
    }
}
