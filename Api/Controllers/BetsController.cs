﻿using Domain.Dtos;
using Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Persistance;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BetsController : ControllerBase
    {
        private readonly DataContext _context;

        public BetsController(DataContext context)
        {
            _context = context;
        }

        // POST: api/Bets
        [HttpPost]
        public async Task<ActionResult<Bet>> PostBet(BetDto betDto)
        {
            var headers = Request.Headers;

            var ruleta = _context.Ruletas.SingleOrDefault(r => r.Id == betDto.RuletaId && r.IsOpen == true);
            if (ruleta == null) return BadRequest("Not found ruleta");
            if (betDto.Number > 36) return BadRequest("Number no valid");

            var bet = new Bet()
            {
                Ruleta = ruleta,
                Type = Bet.GetBetType(number: betDto.Number, colour: betDto.Colour),
                UserId = int.Parse(Request.Headers["x-user-id"]),
                Colour = Bet.GetBetColour(betDto.Colour),
                Number = betDto.Number,
                Money = betDto.Money
            };

            _context.Bets.Add(bet);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetBet", new { id = bet.Id }, bet);
        }

        // DELETE: api/Bets/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Bet>> DeleteBet(int id)
        {
            var bet = await _context.Bets.FindAsync(id);
            if (bet == null)
            {
                return NotFound();
            }

            _context.Bets.Remove(bet);
            await _context.SaveChangesAsync();

            return bet;
        }

        private bool BetExists(int id)
        {
            return _context.Bets.Any(e => e.Id == id);
        }
    }
}
