﻿using Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Persistance;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RuletasController : ControllerBase
    {
        private readonly DataContext _context;

        public RuletasController(DataContext context)
        {
            _context = context;
        }

        // GET: api/Ruletas
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Ruleta>>> GetRuletas()
        {
            return await _context.Ruletas.ToListAsync();
        }

        // PUT: api/Ruletas/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRuleta(int id)
        {
            var ruleta = _context.Ruletas.Single(r => r.Id == id);
            if (ruleta == null)
            {
                return BadRequest();
            }

            ruleta.IsOpen = true;
            _context.Entry(ruleta).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();

                return Ok();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RuletaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }

                return BadRequest();
            }
        }

        // POST: api/Ruletas
        [HttpPost]
        public async Task<ActionResult<Ruleta>> PostRuleta(Ruleta ruleta)
        {
            _context.Ruletas.Add(ruleta);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRuleta", new { id = ruleta.Id }, ruleta);
        }

        // POST: api/Ruletas/5
        [HttpPost("{id}")]
        public async Task<ActionResult<Ruleta>> CloseRuleta(int id)
        {
            var ruleta = _context.Ruletas.SingleOrDefault(r => r.Id == id);
            if (ruleta == null)
            {
                return NotFound();
            }

            if(ruleta.IsOpen == false)
            {
                return BadRequest("Ruleta is closed");
            }

            var winNumber = Ruleta.GetWinnerNumber();
            var bets = _context.Bets.Where(b => b.Ruleta.Id == id).ToList();
            List<BetRuleta> betsRuleta = new List<BetRuleta>();

            foreach(var bet in bets)
            {
                var winMoney = bet.BetMoneyWin(winNumber);

                var betRuleta = new BetRuleta()
                {
                    Bet = bet,
                    Ruleta = ruleta,
                    Winner = (bet.Number == winNumber),
                    Money = winMoney
                };

                betsRuleta.Add(betRuleta);
            }

            ruleta.IsOpen = false;
            _context.Entry(ruleta).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return Ok(betsRuleta);
        }

        private bool RuletaExists(int id)
        {
            return _context.Ruletas.Any(e => e.Id == id);
        }
    }
}
