﻿using Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Persistance
{
    public class DataContext : DbContext
    {
        public DbSet<Ruleta> Ruletas { get; set; }
        public DbSet<Bet> Bets { get; set; }

        public DataContext(DbContextOptions options) : base(options)
        {
        }

    }
}
